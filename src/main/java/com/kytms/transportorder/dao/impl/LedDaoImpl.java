package com.kytms.transportorder.dao.impl;

import com.kytms.core.dao.impl.BaseDaoImpl;
import com.kytms.core.entity.Led;
import com.kytms.transportorder.dao.LedDao;
import org.springframework.stereotype.Repository;

/**
 * 辽宁捷畅物流有限公司 -信息技术中心
 * 陈小龙
 * 分段订单
 *  2018-03-23
 */
@Repository(value = "LedDao")
public class LedDaoImpl extends BaseDaoImpl<Led> implements LedDao<Led> {
}
